﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class PlayerControler : MonoBehaviour {

    Rigidbody rb;
    public float speed;
    int count = 0;

    public Text countText;
    public Text winText;

    bool dead= false;



	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();

	}
	
	// Update is called once per frame
	void Update () {
		if (dead==false && transform.position.y <= -5)
        {

            GetComponent<MeshRenderer>().enabled = false;
            Invoke("RestartLevel", 2f);
        }
	}

    void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    // 
    void FixedUpdate()
    {
        float MoveHorizontal = Input.GetAxis("Horizontal");
        float MoveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(MoveHorizontal, 0f, MoveVertical);

        rb.AddForce(movement * Time.deltaTime * speed);

    }

    void OnTriggerEnter(Collider other)
    {
        print("OnTriggerEnter");
        if (other.gameObject.CompareTag("PickUp")) ;
        {
            Destroy(other.gameObject);
            count += 1;
            SetCountText();

        }
        if (other.gameObject.CompareTag("speedZone")) ;
        {
            speed = speed * 2f;
        }
    }
   
    private void OnTriggerExit(Collider other)
    {
        print("OnTriggerExit");
        if (other.gameObject.CompareTag("speedZone")) ;
        {
            speed = speed / 2f;
        }
    }
     
    void SetCountText()
    {
        countText.text = "Count:" + count.ToString();

        if (count >=12)
        {
            winText.text = "You win!";
        }

    }
}
